<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article id="main-content" class="small-container project-page">
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php if (class_exists('ACF')) : if(get_field('project_type')): ?>
			<div class="website-link"><a href="<?php the_field('project_url'); ?>" target="_blank">View Website</a></div>
			<?php endif; endif; ?>
		</header><!-- .entry-header -->
			
		<div class="entry-content">
			<?php
				echo the_content();
			?>
		</div><!-- .entry-content -->
	</div><!-- #post-## -->
</article>
<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>