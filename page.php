<?php get_header(); ?>

<?php if ( has_post_thumbnail() ) : ?>
<div class="featured-image" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)">
	<?php //the_post_thumbnail(); ?>
</div>
<?php endif; ?>

<article id="main-content" class="container">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
		<div class="entry-content">
			<?php
				echo the_content();
			?>
		</div><!-- .entry-content -->
	</div><!-- #post-## -->
	<?php endwhile; ?>
	<?php endif; ?>
</article>

<?php if (is_active_sidebar('right-sidebar')) : ?>
<!-- <aside id="right-sidebar" class="sidebar">
	<?php //dynamic_sidebar('right-sidebar'); ?>
</aside> -->
<?php endif; ?>
<?php get_footer(); ?>