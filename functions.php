<?php

//Enqueue scripts and styles
function citadel_adding_scripts() {
	// Get the theme data.
	$the_theme     = wp_get_theme();
	$theme_version = $the_theme->get( 'Version' );

	wp_enqueue_style('style', get_template_directory_uri() . '/style.css', array(), $theme_version);
	wp_enqueue_script( 'customjs', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), $theme_version, true );
}
add_action( 'wp_enqueue_scripts', 'citadel_adding_scripts' );

add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo' );
add_theme_support( 'title-tag' );
add_post_type_support( 'page', 'excerpt' );

register_nav_menus( array(
	'primary' 		=> 'Main Menu',
	'secondary' 	=> 'Secondary Menu',
	'social' 		=> 'Social Menu',
	'legalfooter' 	=> 'Legal Footer Menu',
) );

register_sidebar(array(
	'name' 			=> 'Blog Sidebar',
	'id'            => 'blog-sidebar',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

register_sidebar(array(
	'name' 			=> 'Right Sidebar',
	'id'            => 'right-sidebar',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

register_sidebar(array(
	'name' 			=> 'Footer 1',
	'id'            => 'footer-sidebar-one',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

register_sidebar(array(
	'name' 			=> 'Footer 2',
	'id'            => 'footer-sidebar-two',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

register_sidebar(array(
	'name' 			=> 'Footer 3',
	'id'            => 'footer-sidebar-three',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

register_sidebar(array(
	'name' 			=> 'Footer 4',
	'id'            => 'footer-sidebar-four',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => "</div>\n",
	'before_title'  => '<h3 class="widgettitle">',
	'after_title'   => "</h3>\n",
) );

add_filter( 'document_title_separator', 'cc_document_title_separator' );
function cc_document_title_separator( $sep ) {
    $sep = "|";
    return $sep;
}