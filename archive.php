<?php get_header(); ?>

<article id="main-content" class="container">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
		<div class="entry-content">
			<?php
				echo the_content();
			?>
		</div><!-- .entry-content -->
	</div><!-- #post-## -->
	<?php endwhile; ?>
	<?php endif; ?>
</article>

<?php if (is_active_sidebar('blog-sidebar')) : ?>
<aside id="right-sidebar" class="sidebar">
	<?php dynamic_sidebar('blog-sidebar'); ?>
</aside>
<?php endif; ?>
<?php get_footer(); ?>