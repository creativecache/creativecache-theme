<!doctype html>
<html <?php language_attributes(); ?>>
<head itemtype="http://schema.org/Organization" itemscope>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<?php wp_head(); ?>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700|Raleway:300,400,500,600,700" rel="stylesheet" lazyload>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous" lazyload>


</head>
<body <?php body_class(); ?>>
    
    <div class="main-container">

    	<!-- Skip navigation for screen readers -->
    	<a href="#primary-nav" class="skip" role="button">Skip to main navigation</a>
    	<a href="#main-content" class="skip" role="button">Skip to main content</a>
    
    	<!-- Start Header -->
    	<header id="main-header">
    		<!-- Start Branding Header -->
    		<div id="cc-heading" class="container">
    			<?php if ( is_front_page() ) { ?>
    			<h1 class="header-title">
    				<a href="<?php echo site_url(); ?>">
    					<?php echo get_bloginfo( 'name' ); ?>
    				</a>
    			</h1>
    			<?php } else { ?>
    			<div class="header-title">
				    <a href="<?php echo site_url(); ?>"><?php echo get_bloginfo( 'name' ); ?></a><?php if ( is_single() && get_post_type() != 'cc_projects' ) { ?> | <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">Blog</a><?php } ?>
    			</div>
    			<?php } ?>
    		<!-- End Branding Header -->
    
    		<!-- Start Navigation -->
    		<nav id="primary-nav">
    			<a href="#" class="mobile-menu" role="button" aria-label="Navigation Menu"><i class="fas fa-bars fa-fw"></i></a>
    		    <?php if ( has_nav_menu( 'primary' ) ) :
    				wp_nav_menu( array(
    					'theme_location' => 'primary',
    					'menu_id'        => 'primary',
    					'container' 	 => '',
    				) );
                endif; ?>
    		</nav>
    		
    		</div>
    		<!-- End Navigation -->
    	</header>
    	<!-- End Header -->
    
    	<?php if (is_front_page() && has_post_thumbnail()) { ?>
    	<!-- Start Banner Image -->
    	<section id="banner">
    		<?php the_post_thumbnail( 'large' ); ?>
    		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    		<div class="home-content">
    		<?php echo the_content(); ?>
    		</div>
    		<?php endwhile; ?>
    	    <?php endif; ?>
    	</section>
    	<!-- End Banner Image -->
    	<?php } ?>
    
    	<!-- Start Page Content -->
    	<main id="page"<?php if (class_exists('ACF')) : if(get_field('project_type')): ?> class="project-<?php the_field('project_type');?>"<?php endif; endif; ?>>