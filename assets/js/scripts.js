jQuery(document).ready(function($) {
	function footerPush() {
		var footerHeight = $('#footer').outerHeight();
		
		$('.main-container').css('padding-bottom', footerHeight);
	}
	
	$('#primary-nav .menu li.menu-item-has-children > a').each(function() {
		$(this).append('<i class="fas fa-chevron-down"></i>');
	});
	
	$('.mobile-menu').click(function(e) {
		e.preventDefault();
		$('#primary-nav .menu').stop().slideToggle(500);
	});
	
	$('.bar-button.yellow').click(function() {
		if(!$('body').hasClass('page-template-project')) {
		   $(this).parents('.web-window').addClass('minimized');
		}
	})
	
	$('.bar-button.green').click(function() {
		if(!$('body').hasClass('page-template-project')) {
		   $(this).parents('.web-window').removeClass('minimized');
		}
	})
	
	$('#primary-nav .menu li.menu-item-has-children > a').click(function(e) {
		if($('.mobile-menu').is(":visible")) {
			if($(this).next('.sub-menu').is(":hidden")) {
				e.preventDefault();
				$(this).next('.sub-menu').slideDown(500);
			}
		}
	});
	
	$('#page').click(function() {
		if($('.mobile-menu').is(":visible")) {
			if($('#primary-nav .menu').is(":visible")) {
				$('#primary-nav .menu').slideUp(500);
			}
		}
	});
	
	$('#banner').click(function() {
		if($('.mobile-menu').is(":visible")) {
			if($('#primary-nav .menu').is(":visible")) {
				$('#primary-nav .menu').slideUp(500);
			}
		}
	});
	
	$('.expand-all').click(function(e) {
		e.preventDefault();
		if($(this).children('i').hasClass('fa-expand')) {
			$('.web-window').removeClass('minimized');
			$(this).children('i').toggleClass('fa-expand fa-compress');
		} else {
			$('.web-window').addClass('minimized');
			$(this).children('i').toggleClass('fa-expand fa-compress');
		}
	});

	$('#project-type .selected-project > a').click(function(e) {
		e.preventDefault();
		$(this).next().stop().slideToggle(500);
	});

	$('#project-type .all-types a').click(function(e) {
		e.preventDefault();
		var value = $(this).parent().attr('data-value'),
			name = $(this).text();

		$('#project-type .selected-project').attr('data-value', value);
		$('#project-type .selected-project > a').text(name);

		$('#project-type .all-types').slideUp(500);

		if ( value == 'all') {
			$('.website-window').each(function() {
				$(this).show();
			});
		} else {
			$('.website-window').each(function() {
				
				if ($(this).hasClass(value)) {
					$(this).show();
				} else {
					$(this).hide();
				}
			});
		}
	});

	$('#services .cta label').click(function(){
		var input = $(this).prev(),
			name = input.attr('name');
		$('input[name="' + name +'"]').on('change', function() {
		   $('input[name="' + name + '"]').not(this).prop('checked', false);
		});
    });

    $('#service-sort label').click(function() {
    	var name = $(this).attr('for');

    	if (name == 'wordpress') {
    		$('.wordpress-hide').hide();
    		$('.wordpress').show();
    	} else {
    		$('.' + name).toggle();
    		$('.wordpress').hide();
    		$('.wordpress-hide').show();
    	}
    })

    $('#services .detail-expand').click(function() {
    	$(this).closest('.service-table').find('tbody').stop().toggle();
    });

    $('#service-sort input[name="interested-services"]').on('change', function() {
    	if ($('#service-sort input[id="hosting"]:checked').length > 0) {
    		$('#service-sort .hosting-cms').stop().slideDown(500);
    	} else {
    		if ($('#service-sort .hosting-cms').is(':visible')) {
    			$('#service-sort .hosting-cms').stop().slideUp(500);
    		}    		
    	}

		if ($('#service-sort input[name="interested-services"]:checked').length > 0) {
			$('#service-sort .next').prop('disabled', false);
		} else {
			$('#service-sort .next').prop('disabled', true);
		}
	});

    $('#service-sort .next').click(function() {
    	$('#services').stop().slideDown(500);
    	$('#service-sort .back-container').stop().slideDown(500);
    	$('#service-sort .pick').stop().slideUp(500);
    });

    $('#service-sort .back').click(function() {
		$('#services').stop().slideUp(500);
    	$('#service-sort .back-container').stop().slideUp(500);
    	$('#service-sort .pick').stop().slideDown(500);
    });

    $('.project-page .wp-block-gallery').each(function() {
    	$(this).addClass('web-window');
    	$(this).prepend('<div class="top-bar"><div class="bar-button red"></div><button class="bar-button yellow" title="Minimize" role="button" type="button"></button><button class="bar-button green" title="Expand" role="button" type="button"></button></div><div class="gallery-controls"><button class="left"><i class="fas fa-chevron-left"></i></button><button class="right"><i class="fas fa-chevron-right"></i></button></div>')
    	$(this).children('li').first().addClass('visible first');
    	$(this).children('.first').next().addClass('next');
    	$(this).children('li').last().addClass('last prev');
    });

    $('.project-page .wp-block-image').each(function() {
    	$(this).addClass('web-window');
    	$(this).find('img').wrap( '<div class="website"></div>' );
    	$(this).prepend('<div class="top-bar"><div class="bar-button red"></div><button class="bar-button yellow" title="Minimize" role="button" type="button"></button><button class="bar-button green" title="Expand" role="button" type="button"></button></div>')
    });

    $('.project-page .gallery-controls .left').click(function() {
    	var parentGallery = $(this).parents('.wp-block-gallery');

    	parentGallery.find('.visible').removeClass('visible');
    	parentGallery.find('.prev').addClass('visible');
    	parentGallery.find('.prev').removeClass('prev');
    	parentGallery.find('.next').removeClass('next');

    	if(parentGallery.find('.visible').hasClass('first')) {
    		parentGallery.find('.last').addClass('prev');
    		parentGallery.find('.first').next().addClass('next');
    	} else if (parentGallery.find('.visible').hasClass('last')) {
    		parentGallery.find('.first').addClass('next');
    		parentGallery.find('.last').prev().addClass('prev');
    	} else {
    		parentGallery.find('.visible').prev().addClass('prev');
    		parentGallery.find('.visible').next().addClass('next');
    	}
    });

    $('.project-page .gallery-controls .right').click(function() {
    	var parentGallery = $(this).parents('.wp-block-gallery');

    	parentGallery.find('.visible').removeClass('visible');
    	parentGallery.find('.next').addClass('visible');
    	parentGallery.find('.prev').removeClass('prev');
    	parentGallery.find('.next').removeClass('next');

    	if(parentGallery.find('.visible').hasClass('first')) {
    		parentGallery.find('.last').addClass('prev');
    		parentGallery.find('.first').next().addClass('next');
    	} else if (parentGallery.find('.visible').hasClass('last')) {
    		parentGallery.find('.first').addClass('next');
    		parentGallery.find('.last').prev().addClass('prev');
    	} else {
    		parentGallery.find('.visible').prev().addClass('prev');
    		parentGallery.find('.visible').next().addClass('next');
    	}
    });
	
	footerPush();
	
	$(window).resize(function() {
		if($('.mobile-menu').is(":visible")) {
			$('#primary-nav .menu').css('display', 'none');
		} else {
			$('#primary-nav .menu').css('display', 'block');
		}
		
		footerPush();
	});
});