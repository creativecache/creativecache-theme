<?php get_header(); ?>
<article id="main-content" class="container">
	<?php 
		$args = array(
			'post_type' => 'cc_projects',
			'posts_per_page' => 1,
		);
	$the_pages = new WP_Query( $args );

	if( $the_pages->have_posts() ): while( $the_pages->have_posts() ): $the_pages->the_post();
	?>
	<h2>Featured Project</h2>
	<section class="website-window">
		<div class="web-window">
			<div class="top-bar">
				<div class="bar-button red"></div>
				<button class="bar-button yellow" title="Minimize" role="button" type="button"></button>
				<button class="bar-button green" title="Expand" role="button" type="button"></button>
			</div>
			<a href="<?php echo the_permalink(); ?>" title="View <?php echo the_title(); ?>" class="website">
				<?php the_post_thumbnail( 'full' ); ?>
				
			</a>
		</div>
		<div class="project-content">
			<h2 class="entry-title"><?php echo the_title(); ?></h2>
			<?php
				$id = get_the_ID();
				$cats = wp_get_post_terms($id);
				
				if ($cats > 0) :
			?>
				<span class="categories">
					<?php 
						foreach ( $cats as $cat ):
							echo $cat->name;
						endforeach; 
					?>
				</span>
			<?php
				endif;

				if (has_excerpt()) :
			?>
			<div class="project-text"><?php echo the_excerpt(); ?></div>
			<?php endif; ?>
			<a href="<?php echo the_permalink(); ?>">View Project</a>
		</div>
		<div class="more">
			<a href="/projects">All Projects <i class="fas fa-long-arrow-alt-right"></i></a>
		</div>
	</section>
	<?php endwhile; endif; ?>
	<?php wp_reset_postdata(); ?>
</article>

<?php get_footer(); ?>