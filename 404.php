<?php get_header(); ?>

<article id="main-content" class="container">
	<h1>This content cannot be found!</h1>
	<p>Please try one of the menu links above.</p>
</article>

<?php if (is_active_sidebar('right-sidebar')) : ?>
<aside id="right-sidebar" class="sidebar">
	<?php dynamic_sidebar('right-sidebar'); ?>
</aside>
<?php endif; ?>
<?php get_footer(); ?>