<?php
/*
Template Name: All Projects
*/
?>
<?php get_header(); ?>

<article id="main-content" class="container">
    <div class="projects-controls">
    	<h1 class="entry-title">
    	<?php
            $taxonomy = 'projects_categories';
            $terms = get_terms($taxonomy);
        
            if ( $terms && !is_wp_error( $terms ) ) :
        ?>
        <ul id="project-type">
            <li data-value="all" class="selected-project">
            	<a href="#">All</a>
            	<ul class="all-types">
            		<li data-value="all"><a href="#">All</a></li>
            		<?php foreach ( $terms as $term ) { ?>
	            	<li data-value="<?php echo $term->slug; ?>">
		            	<a href="#"><?php echo $term->name; ?></a>
		            </li>
	            	<?php } ?>
            	</ul>
            </li>
        </ul>
        <?php endif; ?>
    	
    	Selected Projects

        <a href="" class="expand-all" title="Toggle Fullscreen" role="button"><i class="fas fa-expand"></i></a>

    	</h1>
        
    </div>
	<?php 
		$args = array(
		'post_type' => 'cc_projects',
		'posts_per_page' => -1,
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',
		);
	$the_pages = new WP_Query( $args );

	if( $the_pages->have_posts() ): while( $the_pages->have_posts() ): $the_pages->the_post();
	    $terms = get_the_terms( $post->ID , 'projects_categories' );
	    $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
		$imgmeta = wp_get_attachment_metadata( $post_thumbnail_id );
	?>
	
	<section class="website-window<?php if ( $terms && !is_wp_error( $terms ) ) : foreach( $terms as $term ):?> <?php echo $term->slug;?><?php endforeach; endif; ?>">
	    <div class="web-window minimized<?php if (class_exists('ACF')) : if(get_field('project_type')): ?> <?php the_field('project_type'); endif; endif; ?>">
	        <div class="top-bar">
	            <div class="bar-button red"></div>
	            <div class="bar-button yellow" title="Minimize" role="button"></div>
	            <div class="bar-button green" title="Expand" role="button"></div>
	        </div>
    	    <div class="website">
    	        <?php the_post_thumbnail( 'full' ); ?>
    	    </div>
	    </div>
	    <div class="project-content">
	        <h2 class="entry-title"><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></h2>
	        <?php if ( $terms && !is_wp_error( $terms ) ) : 
	        ?>
	        <ul class="project-type">
	        	<?php foreach( $terms as $term ):?>
	        		<li><span><?php echo $term->name;?></span></li>
	        	<?php endforeach;?>
	        </ul>
	        <?php endif; ?>
    	    <div class="project-text"><?php echo the_excerpt(); ?></div>
    	    <a href="<?php echo the_permalink(); ?>">View Project</a>
    	</div>
	</section>
	<?php endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_postdata(); ?>
</article>

<?php get_footer(); ?>