        <div class="clear"></div>
    </main>
    <!-- End Page Content -->
    
    <!-- Start Footer -->
    <footer id="footer">
    	<div class="main-footer">
    		<div class="container">
                <h3 class="footer-site-title"><a href="<?php echo site_url(); ?>"><?php echo get_bloginfo( 'name' ); ?></a></h3>
                
                <div class="footer-sidebars">
                    <?php if (is_active_sidebar('footer-sidebar-one')) : ?>
                    <aside id="footer-sidebar-one" class="sidebar">
                    	<?php dynamic_sidebar('footer-sidebar-one'); ?>
                    </aside>
                    <?php endif; ?>
                    
        		    <?php if (is_active_sidebar('footer-sidebar-two')) : ?>
                    <aside id="footer-sidebar-two" class="sidebar">
                    	<?php dynamic_sidebar('footer-sidebar-two'); ?>
                    </aside>
                    <?php endif; ?>
                    
        		    <?php if (is_active_sidebar('footer-sidebar-three')) : ?>
                    <aside id="footer-sidebar-three" class="sidebar">
                    	<?php dynamic_sidebar('footer-sidebar-three'); ?>
                    </aside>
                    <?php endif; ?>
                    
        		    <?php if (is_active_sidebar('footer-sidebar-four')) : ?>
                    <aside id="footer-sidebar-four" class="sidebar">
                    	<?php dynamic_sidebar('footer-sidebar-four'); ?>
                    </aside>
                    <?php endif; ?>
                </div>
                
    			<div class="clear"></div>
    		</div>
    	</div>
    	<div class="secondary-footer">
    		<div class="container">
    			<div id="copyright">
    				<span>Copyright <?php echo date("Y"); ?> <a href="<?php echo site_url(); ?>"><?php echo get_bloginfo( 'name' ); ?></a>. All rights reserved. | Designed, developed, and hosted by <a href="https://creativecache.co/">Creative Cache</a></span>
    			</div>
    			<?php if ( has_nav_menu( 'legalfooter' ) ) : ?>
    			<div id="legal-menu">
    				<?php
    					wp_nav_menu( array(
    						'theme_location' => 'legalfooter',
    						'menu_id'        => 'legalfooter',
    						'container' 	 => '',
    					) ); 
    				?>
    			</div>
    			<?php endif; ?>
    		</div>
    	</div>
    </footer>
    <!-- End Footer -->
</div>
<?php wp_footer(); ?>
</body>
</html>